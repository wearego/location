package jpw.location.locationserver;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.jackson.JacksonFeature;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

/**
 * ApiApplication
 *
 * This application is the basis of the Api Servlet Application configuration.
 * It is only required to generate the swagger definitions, for the eventual
 * JAX-RS interface it may be omitted.
 *
 * @author leeuwencjv
 * @version 0.1
 * @since 7 dec. 2016
 */
public class ApiApplication extends Application {

	public ApiApplication(final URI publishURI) {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setResourcePackage("jpw.location.locationserver");
		beanConfig.setHost(ApiConstants.API_HOST);
		beanConfig.setHost(publishURI.getHost() + (publishURI.getPort() == 80 ? "" : ":" + publishURI.getPort()));
		beanConfig.setScan(true);
		beanConfig.setPrettyPrint(true);
	}

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new HashSet<>();

		resources.add(TheLocationApi.class);
		resources.add(ApiListingResource.class);
		resources.add(SwaggerSerializers.class);
		resources.add(JacksonFeature.class);
		resources.add(CrossOriginRequestFilter.class);

		return resources;
	}
}
