package jpw.location.locationserver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.http.client.utils.URIBuilder;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocationServer {
	/**
	 * File ApiServer.java
	 *
	 * Copyright 2016 JPW
	 */

	private static final String DEFAULT_ATTACH_SCHEME = "http";
	private static final String DEFAULT_ATTACH_HOST = "localhost";
	private static final int DEFAULT_ATTACH_PORT = 8080;

	private static final String DEFAULT_PUBLISH_SCHEME = "http";
	private static final String DEFAULT_PUBLISH_HOST = ApiConstants.API_HOST;
	private static final int DEFAULT_PUBLISH_PORT = ApiConstants.API_PORT;
	private static final String DEFAULT_PUBLISH_PATH = "";

	public static void main(final String[] args) throws URISyntaxException {
		final String configFile = (args.length > 0 ? args[0] : ApiConstants.DEFAULT_PROPERTIES_FILENAME);
		Properties props = new Properties();
		loadProperties(props, configFile);

		final String mySQLPass = System.getenv(ApiConstants.PASSWORD_SETTING_NAME);
		if (mySQLPass != null) {
			System.setProperty(ApiConstants.PASSWORD_SETTING_NAME, mySQLPass);
		}

		final URI attachURI = new URIBuilder().setScheme(DEFAULT_ATTACH_SCHEME).setHost(DEFAULT_ATTACH_HOST)
				.setPort(DEFAULT_ATTACH_PORT).build();

		final URI publishURI = new URIBuilder().setScheme(DEFAULT_PUBLISH_SCHEME).setHost(DEFAULT_PUBLISH_HOST)
				.setPort(DEFAULT_PUBLISH_PORT).setPath(DEFAULT_PUBLISH_PATH).build();

		log.info("Creating server on {} and publish at {}", attachURI, publishURI);
		final ResourceConfig config = ResourceConfig.forApplication(new ApiApplication(publishURI));
		config.register(LocationObjectMapper.class);
		final Server s = JettyHttpContainerFactory.createServer(attachURI, config);
		log.info("Created server {}.", s.toString());
	}

	public static Properties loadProperties(Properties props, String filename) {
		try (InputStream input = new FileInputStream(filename)) {
			props.load(input);
			log.info("Successfully loaded configuration file {}", filename);
		} catch (final FileNotFoundException e) {
			log.warn("Unable to find configuration file {}", filename);
		} catch (final IOException e) {
			log.warn("Unable to read configuration file {}: {}", filename, e.getMessage());
		}
		return props;
	}
}
