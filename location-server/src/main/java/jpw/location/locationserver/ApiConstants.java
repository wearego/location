package jpw.location.locationserver;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;

public class ApiConstants {

	public static final String API_SCHEME = "http";

	public static final String API_HOST = "www.wijben.ga";

	public static final int API_PORT = 80;

	public static final String DEFAULT_PROPERTIES_FILENAME = "config.properties";

	public static final String USER_SETTING_NAME = "user";
	public static final String PASSWORD_SETTING_NAME = "pwd";
	public static final String DB_URL = "db";

	public static final URI getBaseURI() {
		try {
			return new URIBuilder().setScheme(ApiConstants.API_SCHEME).setHost(ApiConstants.API_HOST)
					.setPort(ApiConstants.API_PORT).build();
		} catch (final URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
}
