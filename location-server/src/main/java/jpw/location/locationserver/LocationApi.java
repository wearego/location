package jpw.location.locationserver;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import jpw.location.data.LocationApiResponse;
import jpw.location.data.LocationEntry;

@SwaggerDefinition(info = @Info(description = "API for storing and retrieving locations from the db.", version = "V0.1", title = "Location REST API", termsOfService = "Experimental API, use responsibly.", contact = @Contact(name = "JP", email = "wearego@hotmail.com") , license = @License(name = "Apache 2.0", url = "http://www.apache.org") ) , externalDocs = @ExternalDocs(value = "Location JPW", url = "http://www.duckduckgo.com/") , schemes = {
		SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS })
@Api
@Path("")
public interface LocationApi {

	@GET
	@Path("/location/most-recent")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the most recent location that was sent to the db.", tags = { "receive" })
	public LocationEntry getMostRecentLocation();

	@GET
	@Path("/location/recent-link")
	@Produces(MediaType.TEXT_HTML)
	@ApiOperation(value = "The link to open street map display of the location.", tags = { "receive", "html" })
	public String getRecentLink();

	@GET
	@Path("/location/recent")
	@Produces(MediaType.TEXT_HTML)
	@ApiOperation(value = "The site in a link and an IFrame (not working) of the location.", tags = { "receive", "html" })
	public String getRecent();

	@GET
	@Path("/")
	@Produces(MediaType.TEXT_HTML)
	@ApiOperation(value = "The site and the link of the open street map display of the location.", tags = { "receive",
			"html" })
	public String getRecentRoot();

	@POST
	@Path("/location")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Post a location to the db.", tags = { "send" })
	public LocationApiResponse postLocation(LocationEntry location);

	@POST
	@Path("/location/sample")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Post a sample location to the db.", tags = { "send" })
	public LocationApiResponse postSample();

	@DELETE
	@Path("/location/clear")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Clear entire location DB.", tags = { "clear" })
	public LocationApiResponse clearLocation();

	@GET
	@Path("/test")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(value = "Test that returns a plain text string.", tags = { "test" })
	public String test();
}
