package jpw.location.locationserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.inject.Singleton;

import jpw.location.data.LocationApiResponse;
import jpw.location.data.LocationEntry;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class TheLocationApi implements LocationApi {
	static final String HEAD = "<!DOCTYPE html><html><head><meta charset='UTF-8'><title>Location</title></head>";
	private Connection conn;
	String dbUrl;
	Properties properties;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss (z)");

	public TheLocationApi() {
		properties = LocationServer.loadProperties(System.getProperties(), "config.properties");
		dbUrl= "jdbc:postgresql://" + properties.getProperty(ApiConstants.DB_URL) + "/Location";
	}

	@Override
	public LocationEntry getMostRecentLocation() {
		Statement stmt = null;
		String query = "SELECT \"LocationId\", x, y, z, millis "
				+ "FROM \"Location\".\"Location\" ORDER BY millis DESC;";
		try {
			stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				return new LocationEntry(rs.getLong("millis"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"));
			}
		} catch (SQLException e) {
			log.error("Couldn't execute query.", e);
		}
		return null;
	}

	@Override
	public LocationApiResponse postLocation(LocationEntry location) {
		PreparedStatement stmt = null;
		String query = "INSERT INTO \"Location\".\"Location\" (x, y, z, millis) VALUES (?,?,?,?);";

		try {
			stmt = getConnection().prepareStatement(query);
			stmt.setDouble(1, location.getX());
			stmt.setDouble(2, location.getY());
			stmt.setDouble(3, location.getZ());
			stmt.setLong(4, location.getMillis());
			stmt.executeUpdate();
		} catch (SQLException e) {
			log.error("Couldn't execute query.", e);
		}
		return new LocationApiResponse();
	}

	@Override
	public String test() {
		return "123";
	}

	public Connection getConnection() {
		if (conn != null) {
			try {
				if (!conn.isClosed()) {
					return conn;
				}
			} catch (SQLException e2) {
				log.error("Could not check if connection was closed", e2);
			}
		}

		log.info("Trying to (re)initiate DB connection.");
		if (properties.getProperty("user") == null) {
			log.error("No DB user set.");
		}
		if (properties.getProperty("password") == null) {
			log.error("No password set for DB connection.");
		}

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			log.error("Can't find postgres driver.");
		}

		try {
			conn = DriverManager.getConnection(dbUrl, properties);
		} catch (SQLException e) {
			log.error("Error connecting to db.", e);
		}

		return conn;
	}

	@Override
	public String getRecentLink() {
		LocationEntry location = getMostRecentLocation();
		StringBuilder sb = new StringBuilder();
		sb.append(HEAD + "<body>");
		// http://www.openstreetmap.org/?mlat=47.5433&mlon=-52.8734&zoom=13
		sb.append("<p><a href='https://www.openstreetmap.org/?mlat=");
		sb.append(String.format("%.5g", (location != null) ? location.getX() : 0));
		sb.append("&mlon=" + String.format("%.5g", (location != null) ? location.getY() : 0));
		sb.append("&zoom=13");
		sb.append("'>Location</a></p>");
		sb.append("</body></html>");
		return sb.toString();
	}

	@Override
	public String getRecent() {
		// <meta http-equiv="refresh" content="0; url=http://example.com/" />
		LocationEntry location = getMostRecentLocation();
		StringBuilder sb = new StringBuilder();
		sb.append(HEAD + "<body>");
		sb.append("<p><a href='https://www.openstreetmap.org/?mlat=");
		sb.append(String.format("%.5g", (location != null) ? location.getX() : 0));
		sb.append("&mlon=" + String.format("%.5g", (location != null) ? location.getY() : 0));
		sb.append("&zoom=13");
		sb.append("'>Location</a></p><p>");
		sb.append("<iframe src='https://www.openstreetmap.org/?mlat=");
		sb.append(String.format("%.5g", (location != null) ? location.getX() : 0));
		sb.append("&mlon=" + String.format("%.5g", (location != null) ? location.getY() : 0));
		sb.append("&zoom=13");
		sb.append("'></iframe>");
		sb.append("</p>");
		sb.append("</body></html>");
		return sb.toString();
	}

	@Override
	public String getRecentRoot() {
		LocationEntry location = getMostRecentLocation();
		StringBuilder sb = new StringBuilder();
		sb.append(HEAD + "<body>");
		sb.append("<p><a href='https://www.openstreetmap.org/?mlat=");
		sb.append(String.format("%.5g", (location != null) ? location.getX() : 0));
		sb.append("&mlon=" + String.format("%.5g", (location != null) ? location.getY() : 0));
		sb.append("&zoom=13");
		Instant moment = null;
		if (location != null) {
			moment = Instant.ofEpochMilli(location.getMillis());
		}
		Instant now = Instant.now();
		sb.append(
				"'>Location on " + ((moment != null) ? moment.toString() : "[no location data found]") + "</a></p><p>");
		sb.append("System time now is:" + now.toString() + "</p>");
		sb.append("</body></html>");
		return sb.toString();
	}

	@Override
	public LocationApiResponse clearLocation() {
		Statement stmt = null;
		String delete = "DELETE FROM \"Location\".\"Location\";";
		int result = -1;
		try {
			stmt = getConnection().createStatement();
			result = stmt.executeUpdate(delete);

		} catch (SQLException e) {
			log.error("Couldn't execute query.", e);
		}
		return new LocationApiResponse();
	}

	@Override
	public LocationApiResponse postSample() {
		PreparedStatement stmt = null;
		String query = "INSERT INTO \"Location\".\"Location\" (x, y, z, millis) VALUES (53.002,6.7,0.1, 14900003400001);";
		int result = -1;
		try {
			stmt = getConnection().prepareStatement(query);
			result = stmt.executeUpdate();
		} catch (SQLException e) {
			log.error("Couldn't execute query.", e);
		}
		return new LocationApiResponse();
	}
}
