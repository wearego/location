package jpw.location.locationserver;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * LocationObjectMapper
 *
 * @author wijbengajp
 * @version 0.1
 * @since 15 maart 2017
 */
@Provider
public class LocationObjectMapper implements ContextResolver<ObjectMapper> {

	final ObjectMapper defaultObjectMapper;

	public LocationObjectMapper() {
		this.defaultObjectMapper = new ObjectMapper();
	}

	@Override
	public ObjectMapper getContext(final Class<?> type) {
		return this.defaultObjectMapper;
	}
}
