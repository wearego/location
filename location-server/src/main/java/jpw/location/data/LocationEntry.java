package jpw.location.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationEntry {
	private long millis;
	private double x;
	private double y;
	private double z;
}
